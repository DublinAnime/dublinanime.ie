//file:///home/chris/Desktop/sites/dam_site/dublinanime.ie/forty-index.html
//https://auth0.com/blog/build-an-app-with-vuejs/
var vm;

window.onload = function () {

var AlertComponent = {
  template: '#alert-template',
  props: ['type', 'persist'],
  data() { return { show: true } },
  ready() { if (!this.persist) { setTimeout(() => this.show = false, 4000) } },
}

var LoginComponent = {
  template: '#login-template',
    data() {
        return {
            credentials: {
                username: '',
                password: ''
            },
            errors: {
                non_field_errors: false,
                username: false,
                password: false
            }
        }
    },
    components: { 'alert': AlertComponent },
    methods: {
        submit() {
            var credentials = {
                username: this.credentials.username,
                password: this.credentials.password
            }
            Auth.login(this, credentials, '/')
        }
    },
}



var UserComponent = {
  //template: '<h1 class="red">{{msg}}</h1>',
  props: ['user'],
  template: '<p>{{ user.username }}</p>',
  created: function () {
      var self = this
      this.$http({ url: '/api/users/' + this.$route.params.user_id + '/', method: 'GET' }).then(function (response) {
          self.$data.user = response.data
      }, function (response) {
          alert('ohnoes :O')
      });
  },
};



var API_URL = 'https://api.dublinanime.ie/';
var LOGIN_URL = API_URL + 'rest-auth/login/'//   'auth/token/';
//var SIGNUP_URL = API_URL + 'account/signup';


var Auth = {

    user: {},

    login: function login(context, creds, redirect) {
        var _this = this;
        context.$http.post(LOGIN_URL, JSON.stringify(creds)).then(
          function (data) {
            localStorage.setItem('last_user', creds.username);
            localStorage.setItem('last_user_key', data.body.key);
            _this.setUserDataFromToken(creds.username, data.body.key);
            context.$root.alert('success', "Welcome back, " + _this.user.username + "!");
            context.$root.showModal = false
          }
            /*if (redirect) {
                context.$route.router.go(redirect);
            }*/
        ).catch(
          function (err) { console.log('err!'); context.errors = err.body; }
        );
    },

    /*
    signup: function signup(context, creds) {
        var _this2 = this;

        context.$http.post(SIGNUP_URL, creds, function (data) {
            localStorage.setItem('token', data.token);
            _this2.user.authenticated = true;
            _this2.setUserDataFromToken(data.token);

            if (redirect) {
                context.$route.router.go(redirect);
            }
        }).error(function (err) {
            context.errors = err;
        });
    },
    */

    logout: function logout(context) {
        localStorage.removeItem('last_user');
        localStorage.removeItem('last_user_key');
        this.resetUserData();
    },

    check: function check() {
        var user = localStorage.getItem('last_user');
        if (user) {
          var key = localStorage.getItem('last_user_key');
            this.setUserDataFromToken(user, key);
        } else { this.resetUserData(); }
    },

    setUserDataFromToken: function setUserDataFromToken(user, key) {
        //var data = _utils2.default.jwt_decode(jwt);
        this.user.authenticated = true;
        this.user.id = key;
        this.user.username = user;
    },

    resetUserData: function resetUserData() {
        this.user.authenticated = false;
        this.user.id = 0;
        this.user.username = 'guest';
    },

    getAuthHeader: function getAuthHeader() {
        return {
            //'Authorization': 'Bearer ' + localStorage.getItem('token')
        };
    }

};
Auth.check();

// Plug in the plugins
Vue.use(VueResource);

vm = new Vue({
  el: '#app',
// Define a root component to represent the app
//Vue.extend({
    data() {
        return {
            user: Auth.user,
            alerts: [],
            showModal: false,
            showUserModal: false,
        }
    },
    components: {
        'alert': AlertComponent,
        'login': LoginComponent,
        'user': UserComponent,
        'modal': { template: '#modal-template' },
    },
    methods: {
        alert(type, message) { this.alerts.push({type: type, message: message}); },
        logout() { Auth.logout(this); },
    },
    ready() {

    }
})

// Start the app
//Router.start(App, '#app');

}
